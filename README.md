# **ChatDB** #


### Avtorja: ###
Lea Jarnjak Levstek (63130078),
David Jauk (63130079)


### Opis: ###
Na prvi strani imamo dva dela. Na levi strani imamo polja za registracijo novega uporabnika, na desni pa polja za vstop v klepetalnico. Za registracijo novega uporabnika moramo izpolniti vsa polja, ter gesla se morata ujemati in vsebovati morata vsaj 2 veliki črki, 1 poseben znak, 2 številki in dolgo mora biti najmanj 8 znakov.
![alt text](http://shrani.si/f/1Z/Jr/5je5hcm/screenshot4.jpg "Screenshot1")

Ko se uspešno prijavimo nas aplikacija preusmeri v klepetalnico, kjer na levi strani vidimi vsa sporočila ki so bila poslana, na desni pa vidimo vse trenutno prijavljene uporabnike. S pomočjo gumba Pošlji pošiljamo sporočila, ki smo jih vnesli v vnosno polje. Gumb Osveži nam prikaže vsa nova sporočila od zadnje osvežitve, kot tudi vse novo prijavljene uporabnike. Imamo tudi gumb Odjavi s katerim se odjavimo iz klepetalnice in smo preusmerjeni na prvo stran.
![alt text](http://shrani.si/f/39/sc/UFBi8Hd/screenshot5.jpg "Screenshot2")

Največje težave sva imela pri postavitvi repozitorja ter prvem commitu/pushu, ker sva na ustavrjenem repozitorju naredila Readme.md, kar je tudi povzročilo da se je naredil master branch in nam Visual studio ni dovolil commitat naše aplikacije.


### Razdelitev nalog: ###
Naloge sva si razdelila sledeče:
Lea je pripravila repozitorij in s tem tudi prva deploy-la projekt. Naredila pa je tudi login funkcionalnost ter celotno Chat stran.
David pa je bil zadolžen za registracijo novega uporabnika pod kaj tudi spada validacija vseh podatkov ter spostavitev povezave z bazo.


### Seznam uporabnikov: ###

Username | Password
--- | ---
User | UseR123!
Admin | AdmiN123!