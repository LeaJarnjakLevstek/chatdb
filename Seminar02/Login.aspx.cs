﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Text;

namespace Naloga01_IS
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                LabelOpozorila.Text = "";
            }
        }

        protected void Button1_registreraj_Click(object sender, EventArgs e)
        {
            SqlConnection povezava = new SqlConnection("Data Source = (LocalDB)\\MSSQLLocalDB; AttachDbFilename =|DataDirectory|\\chatdb.mdf; Integrated Security = True");
            {
                SqlCommand insert = new SqlCommand("Insert into Uporabnik(username, ime, geslo) Values(@username, @ime, @geslo)", povezava);
                insert.Parameters.AddWithValue("@username", TextBox1_Uime_R.Text);
                insert.Parameters.AddWithValue("@ime", TextBox2_ime.Text);
                insert.Parameters.AddWithValue("@geslo", GetMd5Hash(TextBox4_Geslo2.Text));


                if (TextBox1_Uime_R.Text != "" && TextBox2_ime.Text != "" && TextBox3_Geslo1.Text != "" && TextBox4_Geslo2.Text != "")
                {
                    if (TextBox3_Geslo1.Text.Equals(TextBox4_Geslo2.Text))
                    {
                        povezava.Open();
                        insert.ExecuteNonQuery();
                        povezava.Close();
                        LabelOpozorila.Text = "Vaš uporabniški račun je bil uspešno ustvarjen!";
                        TextBox1_Uime_R.Text = "";
                        TextBox2_ime.Text = "";
                    }
                    else
                    {
                        LabelOpozorila.Text = "Gesla se ne ujemata";
                    }
                }
                else
                {
                    LabelOpozorila.Text = "Vnesite mankajoče podatke.";
                }
            }
        }

        protected void Button_Prijavi_Click(object sender, EventArgs e)
        {
            SqlConnection povezava = new SqlConnection("Data Source = (LocalDB)\\MSSQLLocalDB; AttachDbFilename =|DataDirectory|\\chatdb.mdf; Integrated Security = True");
            {
                SqlCommand select = new SqlCommand("SELECT COUNT(*) FROM Uporabnik WHERE username = @username AND geslo = @geslo;", povezava);
                select.Parameters.AddWithValue("@username", TextBox5_Uime_L.Text);
                select.Parameters.AddWithValue("@geslo", GetMd5Hash(TextBox8_gesloLogin.Text));
                povezava.Open();
                if ((int)select.ExecuteScalar() == 1) //izpise st vrstic ce kaj najde in ce kaj najde potem je login uspesen
                {

                    Session["s"] = "s";
                    Session["uporabnik"] = TextBox5_Uime_L.Text; //shrani uporabnika v aplikacijsko

                    TextBox5_Uime_L.Text = "";
                    TextBox8_gesloLogin.Text = "";
                    povezava.Close();
                    Response.Redirect("Chat.aspx");

                }
                else
                {
                    LabelOpozorila.Text = "Napačno ime ali geslo!";
                    TextBox5_Uime_L.Text = "";
                    TextBox8_gesloLogin.Text = "";
                }
                
            }
        }

        public string GetMd5Hash(string input)
        {
            System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
                sb.Append(hash[i].ToString("X2"));

            return sb.ToString();
        }
    }
}