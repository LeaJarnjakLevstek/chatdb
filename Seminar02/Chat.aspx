﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Chat.aspx.cs" Inherits="Seminar02.Chat" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Spletni Klepet - 63130078</title>
    <style type="text/css">
        #form1 {
            width: 659px;
            height: 385px;
        }
    </style>
</head>
<body style="height: 378px; width: 651px">
    <form id="form1" runat="server">
    <div>
    
        <asp:Label ID="Label1" runat="server" Text="Prijavleni ste kot "></asp:Label>
        <asp:Label ID="Label2" runat="server"></asp:Label>
        <asp:Label ID="Label3" runat="server" Text="."></asp:Label>
        <br />
    
        <asp:ListBox ID="LB_Pogovor" runat="server" Height="300px" Width="500px"></asp:ListBox>
        <asp:ListBox ID="LB_Uporabniki" runat="server" Height="300px" style="margin-top: 0px" Width="141px"></asp:ListBox>
    
    </div>
        <asp:TextBox ID="TB_Sporocilo" runat="server" Height="70px" Width="420px"></asp:TextBox>
        <asp:Button ID="B_Poslji" runat="server" Height="70px" OnClick="B_Poslji_Click" style="text-align: left" Text="Pošlji" Width="70px" />
        <asp:Button ID="B_Osvezi" runat="server" Height="70px" style="text-align: left" Text="Osveži" Width="70px" OnClick="B_Osvezi_Click" />
        <asp:Button ID="LB_Odjava" runat="server" Height="70px" Text="Odjava" Width="70px" OnClick="LB_Odjava_Click" />
    </form>
</body>
</html>
