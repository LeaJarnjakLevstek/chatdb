﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Text;

namespace Seminar02
{
    public partial class Chat : System.Web.UI.Page
    {

        static ArrayList uporabnik_list = new ArrayList();
        static ArrayList sporocila_list = new ArrayList();
        

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["uporabnik"] == null)
            {

                Response.Redirect("Login.aspx");
            }

            Session["s"] = "s";
            if (!IsPostBack)
            {
                //	Programska	koda,	ki	se	izvede	samo	ob	prvem	prikazu	strani 	 

                Label2.Text = Session["uporabnik"].ToString();

                //Application["Uporabnik"] += Session.SessionID;
                Session["s"] = "s";
                if (Session["uporabnik"] == null)
                {

                    Response.Redirect("Login.aspx");
                }



                if (!uporabnik_list.Contains(Session["uporabnik"]))
                {
                    uporabnik_list.Add(Convert.ToString(Session["uporabnik"].ToString()));
                }

                LB_Pogovor.Items.Clear();
                LB_Uporabniki.Items.Clear();
                sporocila_list = beriSporocila();
                foreach (string i in sporocila_list)        //izpiše sporočilo iz baze
                {
                    LB_Pogovor.Items.Add(i);
                }
                foreach (string i in uporabnik_list)        //izpiše uporabnike
                {
                    LB_Uporabniki.Items.Add(i);
                }



                //LB_Uporabniki.Items.Add((string)Application["Uporabnik"]);
                //LB_Pogovor.Items.Add((string)Application["Sporocila"]);


            }
            if (IsPostBack)
            {
                //	Programska	koda,	ki	se	izvede	samo	ob	ponovnem	prikazu	strani 	
                Session["s"] = "s";


                LB_Pogovor.Items.Clear();
                LB_Uporabniki.Items.Clear();
                //beriSporocila();
                foreach (string i in sporocila_list)        //izpiše sporočilo iz baze
                {
                    LB_Pogovor.Items.Add(i);
                }
                foreach (string i in uporabnik_list)        //izpiše uporabnike
                {
                    LB_Uporabniki.Items.Add(i);
                }

                //LB_Uporabniki.Items.Add((string)Application["Uporabnik"]);
                //LB_Pogovor.Items.Add((string)Application["Sporocila"]);

            }
            //	Programska	koda,	ki	se	izvede	vedno 
            LB_Pogovor.Items.Clear();
            LB_Uporabniki.Items.Clear();
            //beriSporocila();
            foreach (string i in sporocila_list)        //izpiše sporočilo iz baze
            {
                LB_Pogovor.Items.Add(i);
            }
            foreach (string i in uporabnik_list)        //izpiše uporabnike
            {
                LB_Uporabniki.Items.Add(i);
            }
            //LB_Uporabniki.Items.Add((string)Application["Uporabnik"]);
            //LB_Pogovor.Items.Add((string)Application["Sporocila"]);

        }

        protected void B_Poslji_Click(object sender, EventArgs e)
        {


            SqlConnection p = new SqlConnection("Data Source = (LocalDB)\\MSSQLLocalDB; AttachDbFilename =|DataDirectory|\\chatdb.mdf; Integrated Security = True");
            {
                SqlCommand insert = new SqlCommand("Insert into Pogovor(id, username, besedilo) Values(@id, @username, @besedilo)", p);   //shranimo novo sporocilo v bazo
                SqlCommand prestej = new SqlCommand("SELECT COUNT(*) from Pogovor", p);
                p.Open();
                int count = (int)prestej.ExecuteScalar();
                insert.Parameters.AddWithValue("@id", count + 1);
                insert.Parameters.AddWithValue("@username", Session["uporabnik"]);
                insert.Parameters.AddWithValue("@besedilo", TB_Sporocilo.Text);
                int a = insert.ExecuteNonQuery();
                p.Close();

                ///////////////////////////////////////////////////////////////////////
                {
                    //String a = Session["uporabnik"].ToString()+": " + TB_Sporocilo.Text;
                    //sporocila_list.Add(Convert.ToString(a));  //shrani sporocilo 

                    LB_Pogovor.Items.Clear();

                    //LB_Pogovor.Items.Add((string)Application["Sporocila"]);
                    sporocila_list = beriSporocila();
                    foreach (string i in sporocila_list)        //izpiše sporočilo iz baze
                    {
                        LB_Pogovor.Items.Add(i);
                    }
                    TB_Sporocilo.Text = "";
                }
                //////////////////////////////////////////////////////////////////////////
            }

        }

        protected void B_Osvezi_Click(object sender, EventArgs e)
        {
            LB_Pogovor.Items.Clear();
            LB_Uporabniki.Items.Clear();

            //beriSporocila();
            foreach (string i in sporocila_list)        //izpiše sporočilo iz baze
            {
                LB_Pogovor.Items.Add(i);
            }
            foreach (string i in uporabnik_list)        //izpiše uporabnike
            {
                LB_Uporabniki.Items.Add(i);
            }

            //LB_Uporabniki.Items.Add((string)Application["Uporabnik"]);
            //LB_Pogovor.Items.Add((string)Application["Sporocila"]);

        }

        protected void LB_Odjava_Click(object sender, EventArgs e)
        {
            Session["s"] = "s";

            if (Session["uporabnik"] != null)
            {
                uporabnik_list.Remove(Convert.ToString(Session["uporabnik"].ToString()));
            }
            Session.Clear();
            Response.Redirect("Login.aspx");

        }


        protected ArrayList beriSporocila()
        {
            ArrayList sporocila_nov_list = new ArrayList();
            SqlConnection r = new SqlConnection("Data Source = (LocalDB)\\MSSQLLocalDB; AttachDbFilename =|DataDirectory|\\chatdb.mdf; Integrated Security = True");
            {

                SqlCommand pog = new SqlCommand("SELECT username, besedilo from Pogovor", r);
                r.Open();
                String a = "";
                SqlDataReader branje = pog.ExecuteReader();
                while (branje.Read())
                {
                    a = branje.GetString(0) + ": " + branje.GetString(1);

                    sporocila_nov_list.Add(a);
                }
                r.Close();

            }
            return sporocila_nov_list;
        }

    }





}