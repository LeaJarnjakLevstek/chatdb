﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Naloga01_IS.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .newStyle1 {
            font-family: "Gill Sans", "Gill Sans MT", Calibri, "Trebuchet MS", sans-serif;
        }

        .auto-style2 {
            font-family: "Gill Sans", "Gill Sans MT", Calibri, "Trebuchet MS", sans-serif;
            font-size: xx-large;
        }

        .auto-style3 {
            text-align: center;
        }

        .auto-style8 {
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div style="margin-top: 15px" class="auto-style9">

            <asp:Panel ID="Panel1" runat="server" Height="114px">
                <style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
.tg .tg-s6z2{text-align:center}
</style>
<table class="auto-style2" align="center" aria-hidden="True" border="0" style="height: 88px">
  <tr>
    <th class="tg-s6z2" style="width: 50px">
        &nbsp;<br></th>
    <th class="tg-s6z2" style="width: 710px">
        <asp:Label ID="Label1" runat="server" CssClass="auto-style2" Style="font-weight: 700; text-align: center;" Text="  ChatDB"></asp:Label>
        <br></th>
  </tr>
</table>
                <div class="auto-style3">

                </div>
            </asp:Panel>
            <hr class="auto-style12" style="background-color: #000000" />
            <asp:Panel ID="Panel7" runat="server" CssClass="auto-style8" Height="233px">
                <style type="text/css">
                    .tg {
                        border-collapse: collapse;
                        border-spacing: 0;
                    }

                        .tg td {
                            font-family: Arial, sans-serif;
                            font-size: 14px;
                            padding: 10px 5px;
                            border-style: solid;
                            border-width: 1px;
                            overflow: hidden;
                            word-break: normal;
                        }

                        .tg th {
                            font-family: Arial, sans-serif;
                            font-size: 14px;
                            font-weight: normal;
                            padding: 10px 5px;
                            border-style: solid;
                            border-width: 1px;
                            overflow: hidden;
                            word-break: normal;
                        }

                        .tg .tg-s6z2 {
                            text-align: center;
                        }
                </style>
                <table class="auto-style16" align="center" style="height: 333px">
                    <tr>
                        <th class="tg-s6z2" style="width: 314px; height: 38px; text-align: justify; font-weight: normal;">
                            <br>
                            <style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
.tg .tg-s6z2{text-align:center}
.tg .tg-yw4l{vertical-align:top}
</style>
<table class="auto-style12" border="0">
  <tr>
    <th class="tg-s6z2" style="font-size: small; height: 21px"><strong>
        <asp:Label ID="Label2" runat="server" CssClass="newStyle2" style="font-size: large" Text="Registracija"></asp:Label>
        </strong><br class="auto-style10"></th>
    <th class="tg-s6z2" style="height: 21px"><br></th>
  </tr>
  <tr>
    <td class="tg-yw4l" style="height: 37px; font-size: small">
        <asp:Label ID="Label8" runat="server" CssClass="auto-style15" Text="Novi uporabniki"></asp:Label>
        <br />
        <br />
      </td>
    <td class="tg-yw4l" style="height: 37px"></td>
  </tr>
  <tr>
    <td class="tg-yw4l" style="height: 15px; font-size: small;">
        <asp:Label ID="Label4" runat="server" CssClass="newStyle3" Text="Uporabniško ime:"></asp:Label>
      </td>
    <td class="tg-yw4l" style="height: 15px">
        <asp:TextBox ID="TextBox1_Uime_R" runat="server" Height="16px"></asp:TextBox>
      </td>
  </tr>
  <tr>
    <td class="tg-yw4l" style="font-size: small; height: 19px">
        <asp:Label ID="Label10" runat="server" Text="Ime:"></asp:Label>
      </td>
    <td class="tg-yw4l" style="height: 19px">
        <asp:TextBox ID="TextBox2_ime" runat="server" CssClass="auto-style12"></asp:TextBox>
      </td>
  </tr>
  <tr>
    <td class="tg-yw4l" style="font-size: small; height: 9px">
        <asp:Label ID="Label11" runat="server" Text="Geslo:"></asp:Label>
      </td>
    <td class="tg-yw4l" style="height: 9px">
        <asp:TextBox ID="TextBox3_Geslo1" runat="server" CssClass="auto-style13" TextMode="Password"></asp:TextBox>
        <asp:RegularExpressionValidator id="valRegEx" runat="server"
            ControlToValidate="TextBox3_Geslo1"
            ValidationExpression="(?=[^\s]*[^\sa-zA-Z0-9][^\s]*)(?=[^\s]*[a-zA-Z][^\s]*[A-Za-z][^\s]*)(?=[^\s]*\d[^\s]*\d[^\s]*)[^\s]{8,}"
            ErrorMessage="* Neustrezno geslo."
            display="dynamic">*
            </asp:RegularExpressionValidator>
      </td>
  </tr>
  <tr>
    <td class="tg-yw4l" style="font-size: small; height: 11px">
        <asp:Label ID="Label12" runat="server" Text="Geslo:"></asp:Label>
      </td>
    <td class="tg-yw4l" style="height: 11px">
        <asp:TextBox ID="TextBox4_Geslo2" runat="server" CssClass="auto-style14" TextMode="Password"></asp:TextBox>
        <asp:RegularExpressionValidator id="RegularExpressionValidator1" runat="server"
            ControlToValidate="TextBox3_Geslo1"
            ValidationExpression="(?=[^\s]*[^\sa-zA-Z0-9][^\s]*)(?=[^\s]*[a-zA-Z][^\s]*[A-Za-z][^\s]*)(?=[^\s]*\d[^\s]*\d[^\s]*)[^\s]{8,}"
            ErrorMessage="* Neustrezno geslo."
            display="dynamic">*
            </asp:RegularExpressionValidator>
      </td>
  </tr>
  <tr>
    <td class="tg-yw4l" style="font-size: small">&nbsp;</td>
    <td class="tg-yw4l">
        <asp:Button ID="Button1_registreraj" runat="server" CssClass="auto-style10" Text="Registracija" Width="136px" OnClick="Button1_registreraj_Click" />
      </td>
  </tr>
</table>
                            <br />
                            <asp:Label ID="LabelOpozorila" runat="server" ForeColor="Red"></asp:Label>
                            <br />
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionStringUsername %>" InsertCommand="INSERT INTO Uporabnik(username, ime, priimek, geslo) VALUES (@username, @ime, @priimek, @geslo)" SelectCommand="SELECT Uporabnik.* FROM Uporabnik" ProviderName="System.Data.SqlClient">
                                <InsertParameters>
                                    <asp:Parameter Name="username" />
                                    <asp:Parameter Name="ime" />
                                    <asp:Parameter Name="priimek" />
                                    <asp:Parameter Name="geslo" />
                                </InsertParameters>
                            </asp:SqlDataSource>
                            <br />
                        </th>
                        <th class="tg-s6z2" style="width: 82px; height: 38px; font-weight: normal; text-align: left;">
                            <style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
.tg .tg-s6z2{text-align:center}
.tg .tg-yw4l{vertical-align:top}
</style>
                            <style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
.tg .tg-s6z2{text-align:center}
.tg .tg-yw4l{vertical-align:top}
</style>
                          
                            <div style="border-left:1px solid #000;height:300px"></div>
                            &nbsp;
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <br />
                            <br />
                            <br />
                            </th>
                        <th>
                            <table class="auto-style12">
                                <tr>
                                    <th class="tg-s6z2" style="font-size: small; height: 15px; text-align: left; width: 115px;"><strong>
                                        <asp:Label ID="Label5" runat="server" CssClass="newStyle2" style="font-size: large" Text="Prijava"></asp:Label>
                                        </strong>
                                    </th>
                                    <th class="auto-style9"></th>
                                </tr>
                                <tr>
                                    <td class="tg-yw4l" style="width: 115px">
                                        <asp:Label ID="Label9" runat="server" CssClass="auto-style15" style="font-size: small; font-weight: normal;" Text="Obstoječi uporabniki"></asp:Label>
                                        <br />
                                        <br />
                                    </td>
                                    <td class="tg-yw4l">
                                        <br />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tg-yw4l" style="height: 7px; width: 115px;">
                                        <asp:Label ID="Label7" runat="server" CssClass="newStyle3" style="font-size: small; font-weight: normal;" Text="Uporabniško ime:"></asp:Label>
                                    </td>
                                    <td class="tg-yw4l" style="height: 7px">
                                        <asp:TextBox ID="TextBox5_Uime_L" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tg-yw4l" style="width: 115px">
                                        <asp:Label ID="Label13" runat="server" CssClass="auto-style10" Text="Geslo:" style="font-weight: normal; font-size: small;"></asp:Label>
                                    </td>
                                    <td class="tg-yw4l">
                                        <asp:TextBox ID="TextBox8_gesloLogin" runat="server" CssClass="auto-style14" TextMode="Password"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tg-yw4l" style="width: 115px"></td>
                                    <td class="tg-yw4l">
                                        <asp:Button ID="Button_Prijavi" runat="server" CssClass="auto-style10" OnClick="Button_Prijavi_Click" Text="Prijava" Width="136px" />
                                    </td>
                                </tr>
                            </table>
                        </th>
                    </tr>
                </table>
            </asp:Panel>


        </div>
    </form>
</body>
</html>
